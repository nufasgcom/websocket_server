#: -*- coding: utf-8 -*-
import logging
# Настройки  приложения

VERSION = "1.0.0"

DEBUG = True

LOG_LEVEL = logging.INFO

# название приложения
APP_NAME = 'MQTT_WS_SERVICE'

# Настройки MQTT
MQTT_HOST = "10.250.130.192"
MQTT_PORT = 1884
MQTT_KEEPALIVE = 60
MQTT_SUBSCRIPTIONS = [("$SYS/#", 0)]
#MQTT_SUBSCRIPTIONS = []
MQTT_CLIENT_WILL_TOPIC = 'RIP'
MQTT_CLIENT_WILL_PAYMENT = 'Client RIP'

# Настройки WebSocket
WS_PORT = 9191
WS_HOST = '192.168.211.99'

# Настройка логирования
# Имя папки для логирования
LOG_DIR_NAME = '%app_path%/LOGS'
# Имя файла лога
LOG_FILE_NAME = LOG_DIR_NAME + '/' + APP_NAME + '.log'
#: количество дней за которые будет храниться лог
LOGS_DAYS_KEEP = 7

'''
Format  C Type              Python type         Standard size        Notes
x       pad byte            no value
c       char                bytes of length     1
1b      signed char         integer             1                   (1), (2)
B       unsigned char       integer             1                   (2)
?       _Bool               bool                1                   (1)
h       short               integer             2                   (2)
H       unsigned short      integer             2                   (2)
i       int                 integer             4                   (2)
I       unsigned int        integer             4                   (2)
l       long                integer             4                   (2)
L       unsigned long       integer             4                   (2)
q       long long           integer             8                   (2)
Q       unsigned long long  integer             8                   (2)
n       ssize_t             integer                                 (3)
N       size_t              integer                                 (3)
e       (6)                 float               2                   (4)
f       float               float               4                   (4)
d       double              float               8                   (4)
s       char[]              bytes
p       char[]              bytes
P       void *              integer                                  (5)
'''

# Словарь количества байт по типу формата
'''
	Формат -> ключ(тип данных): количество байт
'''
DICT_STRUCT = {
	'x': 1, # заполнитель байта
	'c': 1, # char
	'b': 1, # singed char
	'B': 1, # unsigned char
	'?': 1, # bool - 0 - false, остальное - true
	'h': 2, # short
	'H': 2, # unsigned short
	'i': 4, # int
	'I': 4, # unsigned int
	'l': 4, # long
	'L': 4, # unsigned long
	'q': 8, # long long
	'Q': 8, # unsigned long long
	'n': 1, # ssize_t
	'N': 1, # size_t
	'e': 2, # (6)
	'f': 4, # float
	'd': 8, # double
	's': 1, # char[]
	'p': 1, # char[]
	'P': 1, # void *
	# пользовательские типы
	'10s': 10, # строка из 10 символов
}

# Словарь декодирования тексового представления в формат данных (ключь не должен состоять только из цифр)
'''
Словарь имеет следующий формат:
 Ключ (имя в выходной структуре): тип данных
префикс "^" - в словаре означает что значение будет вставляться в как есть в каждый подзапрос;
"[]" - обозначают цикл, сразу за скобкой идет наименование ключа в котором будет хранится список, 
     в "()" - указывается кол-во циклов, после ":"  указываются ключи считываеммых данных
'''

DICT_DECODER = {
	'PacketType': 'B', # Тип пакета
	'Flags': 'B',# Флаги
	'TimeStampMS': 'Q', # Метка времени в миллисекундах
	'MeasuringCount': 'B', # Кол-во замеров
	'MeasuringTimeShift': 'B', # Смещение замера по времени в миллисекундах от основного времени
	'MeasuringValueInt': 'H', # Значение замера Int.
	'MeasuringValueFloat': 'f',  # Значение замера LongInt.
	'^@': '@', #  Родной порядок байт
	'^=': '=', #  Родной порядок байт стандартный
	'^>': '>', #  Младший порядок байт (little-endian)
	'^<': '<', #  Старший порядок байт (big-endian)
	'^!': '!', #  Сетевой порядок байт (=big-endian)
}

FLOWMETER_FORMAT = '^< PacketType Flags MeasuringCount [ Measurings ( MeasuringCount ) : TimeStampMS MeasuringValueFloat ]'
PRESSURE_FORMAT = '^< PacketType Flags TimeStampMS MeasuringCount [ Measurings ( MeasuringCount ) : MeasuringTimeShift MeasuringValueInt ]'