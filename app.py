#: -*- coding: utf-8 -*-

import config
import logger
from websocket import WebSocket
import paho.mqtt.client as mqtt
import mqtt as mqttutils
import logging


if __name__ == '__main__':
	log_dir_name = logger.correct_path(config.LOG_DIR_NAME)
	log_file_name = logger.correct_path(config.LOG_FILE_NAME)
	logger.start_logs(config.APP_NAME, log_dir_name, log_file_name, config.LOG_LEVEL, config.DEBUG,
	                  config.LOGS_DAYS_KEEP)

	ws = WebSocket(config.APP_NAME, config.WS_PORT, config.WS_HOST)


	publisher = mqttutils.publisher(config.MQTT_HOST, config.MQTT_PORT, config.MQTT_KEEPALIVE)


	subscriber = mqtt.Client()
	subscriber.on_connect = mqttutils.on_connect
	subscriber.on_message = mqttutils.on_message
	subscriber.will_set(config.MQTT_CLIENT_WILL_TOPIC, config.MQTT_CLIENT_WILL_PAYMENT)
	subscriber.enable_logger(logging)
	subscriber.user_data_set({'subscriptions': config.MQTT_SUBSCRIPTIONS, 'WebSocket': ws, 'logger': logger})
	subscriber.connect(config.MQTT_HOST, config.MQTT_PORT, config.MQTT_KEEPALIVE)
	subscriber.loop_start()

	ws.setMQTT(subscriber)

	ws.reactor_run()
