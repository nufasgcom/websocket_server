#: -*- coding: utf-8 -*-

import config
import logging
import struct

def DecodeBytes(bytes, formatString):
	'''
	Преобразование байтов в JSON
	:param bytes: байты для декодирования
	:param formatString:  строка формата
	:return: строка JSON
	'''
	formats = formatString.split(' ')
	result = bytesFromFormat(bytes, formats)
	return result


def GetListCount(listFormat, dict):
	operator = ''
	key = ''
	digit = ''
	try:
		i1 = listFormat.index('(')
		i2 = listFormat.index(')')
		for i in range(i1+1, i2):
			value = listFormat[i]
			if value in ['+', '-', '*', '//', '%']:
				operator = value
			elif value.isdigit():
				digit = int(value)
				di = i
			else:
				key = value
		if key == '':
			return digit
		else:
			if digit != '' and operator != '':
				val = dict[key]
				if operator == '*':
					return val*digit
				elif operator == '+':
					return val+digit
				elif operator == '-':
					if di > 1:
						return val - digit
					else:
						return digit - val
				elif operator == '//': # Целочисленное деление
					if di > 1:
						return val // digit
					else:
						return digit // val
				elif operator == '%': # Остаток от деления
					if di > 1:
						return val % digit
					else:
						return digit % val
			else:
				return dict[key]
	except:
		logging.error('Error read list count')
		return -1

def bytesFromFormat(bytes, formats):
	data = {}  # Словарь для хранения данных
	formatsLen = len(formats)
	i = 0
	endbyte = 0
	multiPrefix = ''
	while i < formatsLen:
		format = formats[i]
		if format in config.DICT_DECODER:
			if format[0] == '^': # Задаем мультипрефикс
				multiPrefix = format[1:]
			else: # обработка ключа
				datatype = config.DICT_DECODER[format]
				datalength = config.DICT_STRUCT[datatype]
				startbyte = endbyte
				endbyte = startbyte + datalength
				databytes = bytes[startbyte: endbyte]
				arr = struct.unpack(multiPrefix + datatype, bytes[startbyte: endbyte])
				value = struct.unpack(multiPrefix + datatype, bytes[startbyte: endbyte])[0]
				#print(datatype, startbyte, endbyte, databytes, value)
				data[format] = value
		elif format == '[': # обработка массива
			ii = i+1
			i1 = -1
			i2 = -0
			while i1 < i2: # поиск конца описания массива
				try:
					i1 = formats.index(']', ii)
				except ValueError as e:
					logging.error('Error read end of list ("]")')
					i1 = formats.len()
					i2 = 0
				try:
					i2 = formats.index('[', ii)
				except ValueError as e:
					i2 = -1
				ii = i1 + 1
			listBegin = i + 1
			listEnd = i1
			listName = formats[listBegin]
			listCount = GetListCount(formats[listBegin: listEnd], data)
			print(listCount)
			try:
				listBegin = formats.index(':', listBegin, listEnd) + 1
			except ValueError as e:
				logging.error('Error read begin of list keys (":")')
				listCount = 0
			data[listName] = []
			for i3 in range(0, listCount):
				frmt = formats[listBegin: listEnd]
				bts = bytes[endbyte:]
				listData, endbyte1 = bytesFromFormat(bts, frmt)
				data[listName].append(listData)
				endbyte = endbyte + endbyte1
			i = listEnd
		i += 1
	return data, endbyte


def coder():
	'''
	Функция для тестирования работоспособности
	'''
	data = (2, 1, 5, (1623145328+0)*1000, 40, (1623145328+60)*1000+60, -45,  (1623145328+120)*1000+120, 50, (1623145328+180)*1000+180, -55, (1623145328+240)*1000+240, 35)
	print(data)
	pref = '<'
	format = 'BBBQfQfQfQfQf'
	bytes = b''
	for i in range(0, len(format)):
		bytes += struct.pack(pref+format[i], data[i])
	print('1')
	print(bytes)
#	print(bytes.decode())
#	print(bytes.decode())
	#print('2')
	#print(struct.pack(format, *data))
	#print(struct.unpack(format, bytes))
	data1 = []
	begin = 0
	for i in range(0, len(format)):
		size = struct.calcsize(format[i])
		db = bytes[begin:begin + size]
		arr = struct.unpack(pref + format[i], bytes[begin:begin + size])
		d = struct.unpack(pref+format[i], bytes[begin:begin+size])[0]
		print(d)
		data1.append(d)
		begin += size
	print(data1)
	data1, endbyte = DecodeBytes(bytes, config.FLOWMETER_FORMAT)
	print(data1)
	x = '\\n'.split('\\')
	y = '\\n'.split('\\')
	#z=bytes.fromhex(r'n')
	c=hex(9)
	print(data1)

if __name__ == '__main__':
	coder()
