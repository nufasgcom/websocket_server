from paho.mqtt import publish

def main():
    msgs = [{'topic': "mqtt/paho/test", 'payload': "[13.15, 12, 11]"},
            {'topic': "mqtt/paho/test", 'payload': "234"}]
    publish.multiple(msgs, hostname="localhost")


if __name__ == "__main__":
    main()