#: -*- coding: utf-8 -*-
from twisted.application import service
from autobahn.twisted.websocket import WebSocketServerFactory
from autobahn.twisted.websocket import WebSocketServerProtocol
import os
if os.name == 'Windows' or os.name == 'nt':
	from twisted.internet import iocpreactor
	iocpreactor.install()
from twisted.internet import reactor
from twisted.application import service
from twisted.python import log
import json
import config
import re
import mqtt as utils

class WSProtocol(WebSocketServerProtocol):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	def onConnect(self, request):
		log.msg(request)

	def onMessage(self, payload, isBinary):
		"""Парсим сообщение и определяем есть ли в нем канал для подписки
		"""
		data = payload.decode("utf-8")
		log.msg(data)
		self.factory.send(self, data)


	def dataReceiveds(self, data):
		"""Парсим сообщение и определяем есть ли в нем канал для подписки
		"""
		data = data.decode("utf-8").splitlines()
		log.msg(data)
		self.factory.send(self, data)



	def notify(self, payload, isBinary):
		#self.transport.write(message)
		if isinstance(payload, str):
			payload = payload.encode()
		self.sendMessage(payload, isBinary)

	def connectionLost(self, reason):
		"""Удаляем ссылку на соединение из фабрики
		"""
		self.factory.removecon(self)


class WSFactory(WebSocketServerFactory):
	def __init__(self, protocol,  *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.protocol = protocol
		self.connections = []
		self.mqtt = None
		self.subscriptions = {}


	def send(self, conn, data):
		"""Отправляет данные брокеру
		"""

		if conn not in self.connections: # Cохраняем соединение
			self.connections.append(conn)

		jsonData = json.loads(data)
		api = jsonData.get('api', '')
		#log.msg(jsonData)
		if api == 'subscribe':
			self.subscribe(jsonData['topic'], conn)
		if api == 'unsubscribe':
			self.mqtt.unsubscribe(jsonData['topic'])
			self.unsubscribe(jsonData['topic'], conn)
		if api == 'payload':
			payload = jsonData['text']
			bytstr = re.match(r'^b"(.*)"b$', payload)
			if bytstr: # Если сообщение в бинарном виде то нужно перевести его из строки в бинарный вид
				try:
					payload = utils.bytesFromStr(bytstr)
				except:
					log.err('Error decode bytesstring')
			try:
				self.mqtt.publish(jsonData['topic'], payload)
			except	Exception as e:
				log.err( 'Error publish to topic "{}" payload "{}" with message {}'.format(jsonData['topic'], payload, str(e)))

	def subscribe (self, topic, conn):
		""" Сохраняем соединение в подписке
		"""
		self.mqtt.subscribe(topic)
		if topic in self.subscriptions:
			self.subscriptions[topic].append(conn)
		else:
			self.subscriptions[topic] = [conn]

	def unsubscribe(self, topic, conn):
		"""Отписывает соединение из подписки
		"""
		if topic in self.subscriptions.keys():
			connections = self.subscriptions[topic]
			if conn in connections:
				connections.remove(conn)
				if len(connections) == 0:
					del self.subscriptions[topic]
					self.mqtt.unsubscribe(topic)

	def removecon(self, conn):
		"""Удаляет соединение из подписок и из списка соединений
		"""
		for topic in list(self.subscriptions.keys()):
			self.unsubscribe(topic, conn)

		if conn in self.connections:
			self.connections.remove(conn)

	def shout(self, data):
		"""Оповещает все соединения которые относятся к каналу
		"""
		#data = json.loads(data)
		outtopic = data['topic']
		js = json.dumps(data)
		#data = data.encode()
		topics = self.subscriptions.keys()
		sent = []
		for topic in topics:
			reg = '^' + topic.replace('#', '.*') + '$'
			if re.match(reg, outtopic):
				connections = self.subscriptions[topic]
				for conn in connections:
					if conn not in sent:
						conn.notify(js, False)
						sent.append(conn)



class WebSocket(service.Service):
	def __init__(self, loggerName, port, host='127.0.0.1'):
		observer = log.PythonLoggingObserver(loggerName=loggerName)
		observer.start()
		self.webSocket_factory = WSFactory(WSProtocol)
		reactor.listenTCP(port, self.webSocket_factory, interface=host)
		self.mqtt = None

	def setMQTT(self, mqtt):
		self.mqtt = mqtt
		self.webSocket_factory.mqtt = mqtt

	def reactor_run(self):
		reactor.run()