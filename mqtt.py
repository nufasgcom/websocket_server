#: -*- coding: utf-8 -*-

import logging
import config
from paho.mqtt import publish
import json
from datetime import datetime
import re
import decoder
import traceback

logger = logging.getLogger(config.APP_NAME)

def on_connect(client, userdata, flags, rc):
	msg = "Connected with result code: %s" % rc
	#logger.info(msg)
	#client.subscribe(userdata['subscriptions'])
	#userdata['WebSocket'].webSocket_factory.shout(msg)

def on_message(client, userdata, msg):
	msgtype = 'default'
	try:
		msg, msgtype = processing(msg)
	except Exception as e:
		print(traceback.format_exc())
		logger.error('MQTT on_message error:"{}" \n with message:{}'.format(str(e), msg))

	try:
		if isinstance(msg.payload, bytes):
			msg.payload = msg.payload.decode('utf-8')
	except Exception as e:
		try:
			msg.payload = '{}'.format(msg.payload)
		except Exception as e:
			str ='MQTT on_message standart bytes decode error:"{}" \n with message:{}'.format(str(e), msg)
			logger.error(str)
			msg.payload = str

		#msg = "%s: %s" % (msg.topic, msg.payload)
	msg = {
		'topic': msg.topic,
		'payload': msg.payload,
		'datetime': datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f"),
		'msgtype': msgtype
	}
	#js = json.dumps(msg)
	#logger.info(js)
	userdata['WebSocket'].webSocket_factory.shout(msg)


def processing(msg): # Обработка сообщения
	msgtype = 'default'
	try:
		if re.fullmatch(r'^usg/.*/p$', msg.topic):
			msg.payload, msgtype = parsePressure(msg.payload) # Получаем измерения давления
			#msgtype = 'Pressure'
		if re.fullmatch(r'^usg/.*/f$', msg.topic):
			msg.payload, msgtype = parseFlowMeter(msg.payload) # Получаем измерения расходомера
			#msgtype = 'FlowMeter'
	finally:
		return msg, msgtype


def parseFlowMeter(bytes): # Парсим давления
	logger.info('FlowMeter')
	logger.info(bytes)
	data, length = decoder.DecodeBytes(bytes, config.FLOWMETER_FORMAT)
	items = []
	for item in data['Measurings']:
		timeStamp = item['TimeStampMS']
		measuringtimestamp = (timeStamp)/1000
		try:
			datetimems = datetime.fromtimestamp(measuringtimestamp)
			item['MeasuringDateTime'] = datetimems.strftime('%Y-%m-%d %H:%M:%S.%f')
		except:
			item['MeasuringDateTime'] = measuringtimestamp
	return data, 'FlowMeter'

def parsePressure(bytes): # Парсим давления
	logger.info('Pressure')
	logger.info(bytes)
	data, length = decoder.DecodeBytes(bytes, config.PRESSURE_FORMAT)
	timeStamp = int(data['TimeStampMS'])
	items = []
	for item in data['Measurings']:
		timeStamp += item['MeasuringTimeShift']
		measuringtimestamp = (timeStamp)/1000
		try:
			datetimems = datetime.fromtimestamp(measuringtimestamp)
			item['MeasuringDateTime'] = datetimems.strftime('%Y-%m-%d %H:%M:%S.%f')
		except:
			item['MeasuringDateTime'] = measuringtimestamp
	return data, 'Pressure'


class publisher():
	def __init__(self, mqtt_host, mqtt_port, mqtt_keep_alive):
		self.mqtt_host = mqtt_host
		self.mqtt_port = mqtt_port
		self.mqtt_keep_alive = mqtt_keep_alive

	def send(self, topic, payload):
		binary = re.fullmatch(r'^b"(.*)"b$')
		if binary:
			payload = binary.group()
		print(payload)
		publish.single(topic=topic, payload=payload, hostname=self.mqtt_host, port=self.mqtt_port, keepalive=self.mqtt_keep_alive)


def bytesFromStr(bytesStr):
	'''
	:param bytesStr: строка в виде последовательности байт u"\\x01\\x00\\x80-\\x00\\xeby\\x01\\x00\\x00\\x05\\x00\\x01\\x00d\\x02\\x00e\\x03\\x00f\\x04\\x00g\\x05\\x00"
	:return: байтовую строку b"\x01\x00\x80-\x00\xeby\x01\x00\x00\x05\x00\x01\x00d\x02\x00e\x03\x00f\x04\x00g\x05\x00"
	'''
	payload = bytesStr.group(1)
	items = payload.split('\\x')
	payload = b""
	for item in items:
		# print(item)
		s = item[0:2]
		b = bytes.fromhex(s)
		i=4
		while i<=len(item):
			x = item[2:i]
			i += 2
			if x == '\\a':
				b += bytes.fromhex('07')
			else:
				if x == '\\b':
					b += bytes.fromhex('08')
				else:
					if x == '\\t':
						b += bytes.fromhex('09')
					else:
						if x == '\\n':
							b += bytes.fromhex('0a')
						else:
							if x == '\\v':
								b += bytes.fromhex('0b')
							else:
								if x == '\\f':
									b += bytes.fromhex('0c')
								else:
									if x == '\\r':
										b += bytes.fromhex('0d')
									else:
										if x == '\\e':
											b += bytes.fromhex('1b')
										else:
											b += x.encode()
		if i-1 <= len(item):
			b += item[i-2:i-1].encode()
		payload += b
		#print(payload)
	return payload