#: -*- coding: utf-8 -*-

from logging.handlers import TimedRotatingFileHandler
import os
import errno
import logging

mnemonics_ = {'%app_path%': os.path.abspath(os.path.dirname(__file__))}

def start_logs(APP_NAME, LOGS_DIR, LOG_FILE_NAME, LOG_LEVEL = logging.INFO, DEBUG = False, LOGS_DAYS_KEEP = 0):
	""" Настраивает журналирование. В зависимости от режима работы выбирает
	способ журналирования.Создает папку для хранения логов если необходимо
	"""
	if DEBUG:
		logging.basicConfig(level=logging.DEBUG)
	else:
		#: прежде всего создаем папку для хранения, если ее нет
		try:
			os.makedirs(LOGS_DIR)
		except OSError as exception:
			if exception.errno != errno.EEXIST:
				raise
		#: делим логи по дням
		handler = TimedRotatingFileHandler(LOG_FILE_NAME,
		                                   when="midnight",
		                                   backupCount=LOGS_DAYS_KEEP)
		format = '%(asctime)s: %(levelname)-6s %(message)s'
		log_format=logging.Formatter(fmt=format, datefmt='%y-%m-%d %H:%M:%S')
		handler.setFormatter(log_format)

		logger = logging.getLogger(APP_NAME)
		logger.setLevel(LOG_LEVEL)
		logger.addHandler(handler)
		#: start message
		logger.info('='*79)

def correct_path(path, mnemonics=mnemonics_):
	"""
	Изменяет путь согластно мнемоник
	:param path: Путь содержащий мнемоники
	:param mnemonics: словарь где ключ - код мнемоники, значение - для данного кода (ключа)
	:return: Исправленная строка
	"""
	for key in mnemonics:
		path = path.replace(key, mnemonics[key])
	return path
